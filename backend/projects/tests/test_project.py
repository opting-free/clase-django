from django.test import TestCase, Client
from rest_framework import status
from projects.models import Project
from django.urls import reverse
import json

class TestProject(TestCase):
    """this test make the following atemps:
    1) create a valid project
    """
    client = Client()

    def setUp(self):
        self.valid_payload = {
        'name': 'proyecto cool',
        'description': 'asdasdasd'
        }
        self.invalid_payload = {
            'namesasdad': 'aasdasdsad'
        }
    
    def test_create_valid_project(self):

        response = self.client.post(
            reverse('project_list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_create_invalid_project(self):

        response = self.client.post(
            reverse('project_list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_get_projects(self):
        Project.objects.create(
            name = "proyecto 1",
            description = "que paza"
        )
        Project.objects.create(
            name = "proyecto 1",
            description = "que paza"
        )
        response = self.client.get(
            reverse('project_list'),
            content_type='application/json'
        )
        projects = json.loads(response.content)
        length_projects = len(projects)
        self.assertEqual(length_projects, 2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_project_by_id(self):
        project = Project.objects.create(**self.valid_payload)
        response = self.client.get(
            reverse('project_detail', kwargs={'project_id': project.id}),
            content_type='application/json'
        )
        project_response = json.loads(response.content)
        self.assertEqual(project.name, project_response['name'])

    def test_delete_project_by_id(self):
        project = Project.objects.create(**self.valid_payload)
        response = self.client.delete(
            reverse('project_detail', kwargs={'project_id': project.id}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_patch_project_by_id(self):
        project = Project.objects.create(**self.valid_payload)
        custom_payload = {
            'name': 'nombre distinto'
        }
        response = self.client.patch(
            reverse('project_detail', kwargs={'project_id': project.id}),
            data=json.dumps(custom_payload),
            content_type='application/json'
        )
        project_response = json.loads(response.content)
        self.assertEqual(custom_payload['name'], project_response['name'])
