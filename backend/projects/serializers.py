from rest_framework import serializers
from projects.models import *

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('__all__')
    


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('__all__')
    
    def create(self, validate_data):
        project = Project.objects.create(**validate_data)
        return project
    
    def update(self, project, validate_data):
        project.name = validate_data.get('name', project.name)
        project.description = validate_data.get('description', project.description)
        project.save()
        return project