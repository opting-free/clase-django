from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=False, null=False)
    expiration_date = models.DateTimeField(null=True)
    creation_date = models.DateTimeField(auto_now_add = True)

class Task(models.Model):
    name = models.CharField(max_length=255)
    project = models.ForeignKey(Project, related_name = 'tasks', on_delete = models.CASCADE)
    expiration_date = models.DateTimeField(null=True)
    creation_date = models.DateTimeField(auto_now_add = True)



