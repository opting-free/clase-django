from django.urls import path
from projects.views import ProjectListView, ProjectDetailView

urlpatterns = [
    path(
        'projects',
        ProjectListView.as_view(),
        name = 'project_list'
    ),
    path(
        'projects/<int:project_id>',
        ProjectDetailView.as_view(),
        name = 'project_detail'
    )
]
